<?php
	session_start();
	if (isset($_SESSION['utente'])) {
		
		$_SESSION['refresh']+=1;
	}
	$GLOBALS['Title'] = "EZ";
?>
<!-- Pagina principale del sito -->
<!DOCTYPE html>
<html class="h-100">
	<head >
		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<title><?php echo $GLOBALS['Title']?></title>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<link rel='stylesheet' type='text/css' media='screen' href='css/main.css'>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<script src="JS/caricamentoProgresso.js"></script>
	</head>
	<body class="text-white d-flex flex-column h-100">
	<?php 
		//  inserisco la navbar
		require_once(realpath(__DIR__).'/inc/header.inc.php');
		if (isset($_SESSION['refresh']) && $_SESSION['refresh']==1) {
			if(isset($_SESSION['firstTime'])){				
	?>
			<!-- banner benvenuto appena iscritto -->
			<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
			<symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
				<path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
			</symbol>
			</svg>
			<div class="alert alert-success d-flex align-items-center mb-0" role="alert">
				<svg class="bi flex-shrink-0 me-2" width="24" height="24"><use xlink:href="#check-circle-fill"/></svg>				
				<div>
					Benvenuto <strong> <u> <?php echo $_SESSION['utente'] ?></u>  </strong> grazie per esserti iscritto! Da ora i progressi nelle pagine lezioni saranno salvati!
				</div>
			</div>

	<?php 
			}else{
	?>
	<!-- banner di benvenuto a login fatto -->
		<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
			<symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
				<path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
			</symbol>
		</svg>
		<div class="alert alert-success d-flex align-items-center mb-0" role="alert">
			<svg class="bi flex-shrink-0 me-2" width="24" height="24"><use xlink:href="#check-circle-fill"/></svg>			
			<div>
				Bentornato <strong> <u> <?php echo $_SESSION['utente'] ?></u>  </strong> i tuoi progressi sono stati caricati con successo!
			</div>
		</div>
		<script>
			aggiornaProgressoLocale(<?php echo json_encode($_SESSION['progresso']) ?>);
		</script>
	<?php
			}
		}
	?>
	<!-- contenuto della pagina principale con un intro al sito -->
		<main class="container-fluid h-100 overflow-auto" id="indexPage">
			<div class="row h-50">
				<div class="col h-100">
					<div class="container-fluid position-relative h-100 pt-2">
						<div class="card border-matrix w-75 position-relative top-0 start-50 translate-middle-x bg-transparent-1 border-matrix mt-4" > 
							<div class="card-header">			
								<p class="card-title display-5 text-center text-matrix"> Benvenuto in <strong>EZ</strong> learn to program! </p>
							</div>
							<div class="card-body bg-transparent-1">
								<p class="card-text text-center text-matrix"> 
									<!-- PHP e JS possono esser link alle pagine -->
									Qui potrai trovare tutorials per imparare linguaggi di programmazione web come PHP e JavaScript, ma non solo! <br>
									<br> 
									Visita la pagina delle video Lezioni per l'accesso immediato a video tutorials su PHP, JavaScript e altri linguaggi di programmazione!
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row h-50">
				<div class="col"> 
					
				</div>
			</div>

		</main>
		<!-- script per l'animazione della rotellina nella navbar -->
		<script>
			 function showSpin(nome){
                 $('#'+nome).show('slow').delay(1600).hide('slow');
                };
		</script>
	<?php	
		require_once(realpath(__DIR__).'/inc/footer.inc.php');
	?> 