        <!-- navbar per schermata insLez -->
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-black">
                <div class="container-fluid">
                    <a class="navbar-brand text-matrix" href="../../index.php">EZ</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse text-matrix" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link text-matrix" href="intro.php">Lessons Intro</a>
                            </li>
                            <li class="nav-item dropdown ">
                                <a class="nav-link dropdown-toggle text-matrix" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Languages
                                </a>
                                <ul class="dropdown-menu  bg-black text-matrix" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item text-matrix" href="../../Lezioni.php?id=1">JavaScript</a></li>
                                    <li><a class="dropdown-item text-matrix" href="../../Lezioni.php?id=2">PHP</a></li>
                                    <li><hr class="dropdown-divider"></li>
                                    <li><a class="dropdown-item text-matrix" href="../../Lezioni.php?id=3">Video Lessons</a></li>
                                </ul>
                            </li>
                        </ul>
                        <!-- se l'utente è anche master mostro il pulsante per la pagina all'inserimento delle lezioni -->
                        <?php
                            if(isset($_SESSION['utente'])){
                                
                        ?>
                                <div class="me-2 text-matrix">
                                    <span class="spinner-border spinner-border-sm " style="display:none" role="status" aria-hidden="true" id="spin-logout"> </span>
                                    <a class="btn btn-outline-success text-matrix" onclick="showSpin('spin-logout');inviaProgresso();" href="../auth/logout.php"  id="log-out"> Logout </a>
                                </div>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </nav>
        </header>