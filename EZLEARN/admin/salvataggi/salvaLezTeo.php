<?php
    session_start();
    if ($_SESSION['master']==0) {
        header("Location: ../../index.php");
        die();
    }
    include_once(realpath(__DIR__)."../../../conf/config.inc.php");
    $codice = $_SESSION['codice'];
    $sql= 'select `numero_lezione`, `titolo_lezione` from lezioni_teoria where tipo_lezione_codice_lezione = "'.$codice.'" order by numero_lezione asc;';
    $result = mysqli_query($conn, $sql);
    $errorNumLection=false;
    $errorCorpo=false;
    $errorTitolo=false;
    $riuscito=false;
    while ($a = $result->fetch_assoc()) {
        if (!preg_match("/^[0-9]{1,2}[.][0-9]{2}$/",$_POST['numbLess'])||$a['numero_lezione']===$_POST['numbLess']) {
            $errorNumLection=true;
            break;
        }
    }
    if (!$errorNumLection) {
        if ($_POST['titoloLez']&&preg_match("/[A-Z0-9 ]{4,20}/i",$_POST['titoloLez'])) {
            if (isset($_POST['corpo'])) {
                $corpo = urlencode($_POST['corpo']);
                $num = $_POST['numbLess'];
                $tit=$_POST['titoloLez'];
                $sql="insert INTO `mydb`.`lezioni_teoria` (`contenutoLezione`, `tipo_lezione_codice_lezione`, `numero_lezione`, `titolo_lezione`) VALUES ('{\"corpo\":\"$corpo\",\"titolo\":\"$tit\"}', '$codice', '$num', '$tit');";
                mysqli_query($conn, $sql);
                $riuscito=true;
            }else {
                $errorCorpo=true;
            }
        }else {
            $errorTitolo=true;
        }
    }
    $errori = array("errorNumLection"=>$errorNumLection, "errorCorpo"=>$errorCorpo, "errorTitolo"=>$errorTitolo);
    $_SESSION['errors']=$errori;
    $_SESSION["caricamentoOK"]= $riuscito;
    $conn -> close();
    header("Location: ../insLesson.php");

    
