<?php
    session_start();
    if ($_SESSION['master']==0) {
        header("Location: ../index.php");
        die();
    }
    include_once(realpath(__DIR__)."../../conf/config.inc.php");
    $sql = 'select * from mydb.tipo_lezione;';
    $result = mysqli_query($conn, $sql);
    $GLOBALS['Title']="Add Lessons";
?>

<!-- pagina inserimento lezioni -->
<!DOCTYPE html>
<html class="h-100">
	<head >
		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<title>EZ - <?php echo($GLOBALS['Title']) ?></title>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<link rel='stylesheet' type='text/css' media='screen' href='../css/main.css'>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <!-- <script src="/JS/caricamentoProgresso.js"></script> -->
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
	</head>
	<body class="bg-primary text-white d-flex flex-column h-100" id="wrapper">
<!-- navbar -->
<?php 
    require_once(realpath(__DIR__).'/inc/header.inc.php');
    if (isset($_SESSION['errors'])) {
        $errori = $_SESSION['errors'];
        $banner = false;
        foreach ($errori as $key => $value){
            if($value){
                $banner=true;
                break;
            }
        }
        if ($banner) {
           
?>  
<!-- banner creazione fallita -->
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
            </symbol>
        </svg>
        <div class="alert alert-danger d-flex align-items-center mb-0" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24"><use xlink:href="#exclamation-triangle-fill"/></svg>
            <div>
            <?php 
                if ($errori['errorNumLection']) {
                    echo 'numero lezione già in uso!<br>';
                } 
                if ($errori['errorLink']) {
                    echo 'link inserito non valido!<br>';
                }
                if ($errori['errorTitolo']) {
                    echo 'titolo della lezione troppo lungo o contenente caratteri proibiti!<br>';
                }
                if ($errori['errorCorpo']) {
                    echo 'Il corpo della lezione non può essere vuoto!<br>';
                }
                if ($errori['output']) {
                    echo 'L\'output inserito non è valido!<br>';
                }             
            ?>   
            </div>
        </div>
<?php
        }
    $_SESSION["errors"]= array();
    }
    if ($_SESSION["caricamentoOK"]) {
?>
<!-- banner inserimento riuscito -->
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
        </symbol>
        </svg>
        <div class="alert alert-success d-flex align-items-center" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24"><use xlink:href="#check-circle-fill"/></svg>
            <div>
                caricamento avvenuto con successo            
            </div>
        </div>
<?php
    $_SESSION["caricamentoOK"]= false;
    }
?>
<!-- corpo centrale della pagina, richiede di scegliere il linguaggio ed un tipo di lezione -->
    <main class="container-fluid h-100 overflow-auto">
        <div class='row-cols-1 h-25 bg-transparent-2 p-4' id="selTipoLezDiv">
            <label for="selLing" class="text-matrix " >Scegliere linguaggio della lezione</label>
            <select class="form-select" aria-label="linguaggio della lezione" id='selLing' required>
                <option selected>...</option>
<?php
    
    $i=0;
    while ($a = $result->fetch_assoc()) {
        $i += 1;
        echo '<option value="'.$a['codice_lezione'].'">'.$a['descrizione'].'</option>';
    }
?>
            </select>
            <label for="selTipoLez" class="text-matrix" >Scegliere il tipo di lezione da inserire</label>
            <select class="form-select" id="selTipoLez" aria-label="Default select example" disabled> 
                <option selected>...</option>
                <option value="teo">Teoria</option>
                <option value="ese">Esercizi</option>
                <option value="vid">video</option>
            </select>
        </div>
        <div class="row-cols-1 h-75">
            <div id='insLezioni'>
            </div>
        </div> 
    </main>

<script>
    var selettoreTipo = document.getElementById('selTipoLez');
    var selettoreLing = document.getElementById('selLing');
    selettoreLing.onchange = (event) => {
        selettoreTipo.toggleAttribute("disabled");
        selettoreLing.toggleAttribute("disabled");
    }
    selettoreTipo.onchange = (event) => {
        if (selettoreTipo.value==='teo') {
            $('#insLezioni').load('insTeor.php', {codice: selettoreLing.value });
            selettoreTipo.toggleAttribute("disabled");
        }else if(selettoreTipo.value==='ese'){
            $('#insLezioni').load('insEse.php', {codice: selettoreLing.value });
            selettoreTipo.toggleAttribute("disabled");
        }else{
            $('#insLezioni').load('insVid.php', {codice: selettoreLing.value })
                
            selettoreTipo.toggleAttribute("disabled");
        }
    }
    function showSpin(nome){
        $('#'+nome).show('slow').delay(1600).hide('slow');
    };
    function inviaProgresso(){
        var daSalv = {};
        for (let index = 0;  index < localStorage.length; index++){
            daSalv[""+localStorage.key(index)] = localStorage.getItem(localStorage.key(index)); 
        }
        localStorage.clear();
        $.ajax({
            url: '../azioni/updateDB.php',
            type: 'POST',
            data:  {"salvataggio" : JSON.stringify(daSalv)}
        })
        
    }
</script>
<!-- footer -->
<?php 

    require_once(realpath(__DIR__).'../../inc/footer.inc.php');
?>


