<?php
    session_start();
    include_once(realpath(__DIR__)."../../conf/config.inc.php");
    $codice = $_POST['codice'];
    $sql= 'select `numero_lezione`, `titolo_lezione` from lezioni_teoria where tipo_lezione_codice_lezione = "'.$codice.'" order by numero_lezione asc;';
    $result = mysqli_query($conn, $sql);
    $_SESSION['codice'] = $_POST['codice'];
?>
<div id="tabellaLez" class="overflow-auto">
    <table class="table bg-transparent-2 text-matrix border-success">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Titolo Lezione</th>
        </tr>
    </thead>
    <tbody>
    <?php
        while ($contTabVid =	$result->fetch_assoc()) {
    ?>
        <tr>
        <th scope="row"><?php echo $contTabVid['numero_lezione'] ?></th>
        <td><?php echo $contTabVid['titolo_lezione'] ?></td>
        </tr>
    <?php
        }
    ?>
    </tbody>
    </table>
</div>
<div id="input_dati_lezione">
    <form class="needs-validation bg-transparent-2 p-4" action="salvataggi/salvaLezTeo.php" method="POST" id="teoForm" novalidate>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                <label for="numbLess" class="text-matrix">Numero della lezione</label>
                <input type="number" min="1.00" step="0.01" name="numbLess" class="form-control" id="numbLess" placeholder="1.00" required>
                <div class="invalid-feedback">
                   numero della lezione non valido!
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                <label for="titoloLez" class="text-matrix">Titolo della lezione [Max 20]</label>
                <input type="text" maxlength="20" pattern="[A-Za-z0-9 ]{4,20}" name="titoloLez" class="form-control" id="titoloLez" placeholder="Titolo" aria-describedby="inputGroupPrepend" required>
                <div class="invalid-feedback">
                    inserire un titolo adeguato! (Min 4, Max 20 caratteri)
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                <label for="corpo" class="text-matrix">Corpo della lezione</label>
                <textarea  name="corpo" class="form-control" id="corpo" form="teoForm" rows="5" placeholder="Inserire il corpo della lezione qui, prferibilmente provvisto di html tags" required></textarea>                
            </div>
        </div>
        <button class="btn btn-success" type="submit"><span class="spinner-border spinner-border-sm " style="display:none" role="status" aria-hidden="true" id="spin"></span>Send</button>
    </form>
</div>
<script>
    var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function(form) {
    form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
        }
        form.classList.add('was-validated');
    }, false);
    });
</script>