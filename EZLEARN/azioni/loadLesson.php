<?php
	include_once(realpath(__DIR__)."../../conf/config.inc.php");
	session_start();
	$tipo = $_POST['tipo'];
	
	$tipo1 = explode('-' , $tipo);

	$tipoLez 	= 	$tipo1[0];
	$idLez		= 	$tipo1[1];
	$codiceLez	= 	$tipo1[2];
	
	if($tipoLez == "teo"){
		$sql = 
			"select `contenutoLezione` from lezioni_teoria
				where `tipo_lezione_codice_lezione` = \"$codiceLez\" and `numero_lezione` = \"$idLez\"";

	}elseif ($tipoLez == "ese") {
		$sql = 
		"select `contenutoLezione` from esercizi_lezioni
				where `tipo_lezione_codice_lezione` = \"$codiceLez\" and `numero_lezione` = \"$idLez\"";

	}else {
		$sql = 
		"select `contenutoLezione` from video_lezioni
				where `tipo_lezione_codice_lezione` = \"$codiceLez\" and `numero_lezione` = \"$idLez\"";
	}
	
 	$result = mysqli_query($conn, $sql);

	$contenutoLezione	=	$result->fetch_assoc();
	$contenuto = json_decode($contenutoLezione['contenutoLezione'], true);
	if ($tipoLez!= 'vid') {
		include_once(realpath(__DIR__)."../../corpiLezione/corpoLezione.php");
	}else {
		include_once(realpath(__DIR__)."../../corpiLezione/corpoLezVid.php");
	}

	if(isset($contenuto['titolo'])){
		$GLOBALS['Title'] = $contenuto['titolo'];
		echo ('<script> aggiornaTitolo("EZ - '.$GLOBALS["Title"].'");
			</script>');
	}
?>

	
<!-- se carico un'esercizio, setto l'output aspettato dell'esercizio -->
<!-- se carico un video, setto l'editor in base al linguaggio della lezione -->
<?php
	if($tipoLez=='ese' && $contenuto['output'] != 'null'){
		
		echo ('<script> setExpectedOut("'.urldecode($contenuto['output']).'"); 

		</script>');	
		
	}elseif ($tipoLez=='vid') {
		if ($codiceLez=="JS") {
			echo ('	<script> 
							setLanguage("ace/mode/javascript"); 
							setLangId("63");
							setEditorText("//Scrivi qui il codice!");
							resetEditRisp();
							resetExpectedOut();
					</script>');
		}else {
			echo ('	<script> 
							setLanguage("ace/mode/php"); 
							setLangId("68");
							setEditorText("<?php #Scrivi qui il codice! ?>");
							resetEditRisp();
							resetExpectedOut();
					</script>');
		}		
	}else {
		echo ('<script> resetExpectedOut(); 

		</script>');
	}
	
$conn -> close();

?>