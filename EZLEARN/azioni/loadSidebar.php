<?php
    include_once(realpath(__DIR__)."../../conf/config.inc.php");
    if ($id != '3') {
        $sql = 
                "select `titolo_lezione`, `numero_lezione` from lezioni_teoria
                where `tipo_lezione_codice_lezione` = \"$cod\" order by `numero_lezione`;";

        $res_prepare = mysqli_query($conn, $sql);

        while($row=$res_prepare->fetch_assoc()) {
            $key = $row['numero_lezione'];
            $value = $row['titolo_lezione'];
            $teoria[$key] = $value;
        }

        $sql = 
                "select `titolo_lezione`, `numero_lezione` from esercizi_lezioni
                where `tipo_lezione_codice_lezione` = \"$cod\" order by `numero_lezione`;";

        $res_prepare = mysqli_query($conn, $sql);

        while($row=$res_prepare->fetch_assoc()) {
            $key = $row['numero_lezione'];
            $value = $row['titolo_lezione'];
            $esercizi[$key] = $value;
        }

    } else {
        $sql = "select `tipo_lezione_codice_lezione`, `numero_lezione`,
                `titolo_Lezione`  from video_lezioni order by `numero_lezione`;";

        $res_prepare = mysqli_query($conn, $sql);

        while($row=$res_prepare->fetch_assoc()) {
            $key = $row['numero_lezione'];
            $value = $row['titolo_Lezione'];
            $genere = $row['tipo_lezione_codice_lezione'];
            $videoLezioni[$genere][$key] = $value;
        }
        
    }
    
    $conn -> close();
    
?>