<!-- pagina principale delle lezioni -->
<?php 
 session_start();
// considero la richiseta della pagina in base al link nella nav-bar selezionato e inizializzo le variabili perchè  corrispondano alla visualizzazione del contenuto
// desiderata 
    $id = $_REQUEST['id'];

    $testoDivRisult = "Qui vedi il risultato del codice!";

    if ($id == '1') {
        $GLOBALS['Title'] = 'JavaScript';
        $language = 'ace/mode/javascript';
        $lanID = 63;
        $cod = 'JS';
        $editorText= '//Scrivi qui il codice!';
    }elseif ($id == '2') {
        $GLOBALS['Title'] = 'PHP';
        $language = 'ace/mode/php';
        $lanID = 68;
        $cod = 'PHP';
        $editorText= '<?php #Scrivi qui il codice! ?>';
    }elseif($id == '3'){
        $GLOBALS['Title'] = 'VideoLezioni';
        $language = 'ace/mode/php';
        $lanID = 68;
        $cod = 'VID';
        $editorText= '<?php #Scrivi qui il codice! ?>';
    }
// carico in sequenza l'head, la nav-bar e preparo ciò che andrà nella sidebar
    require_once(realpath(__DIR__).'/inc/head.inc.php');
    require_once(realpath(__DIR__).'/inc/header.inc.php');
    require_once(realpath(__DIR__).'/azioni/loadSidebar.php');
?>
<!-- modal per il controllo dell'esercizio -->
    <!-- modal esercizio riusito giusto -->
<div class="modal text-matrix" tabindex="-1" id="modal_riuscita">
  <div class="modal-dialog bg-transparent-1 border-success ">
    <div class="modal-content bg-transparent-1 border-success ">
      <div class="modal-header bg-transparent-1 border-success ">
        <h5 class="modal-title bg-transparent-1 border-success ">Complimenti</h5>
        <button type="button" class="btn-close btn-success" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body bg-transparent-1 border-success ">
        <p class="text-md-center h3"><img src="backgrounds/icon/yeah.gif" width="50" hight="50"></img>BEN FATTO!<img src="backgrounds/icon/yeah.gif" width="50" hight="50"></img></p>
      </div>
      <div class="modal-footer bg-transparent-1 border-success ">
        <button type="button" class="btn btn-success text-matrix" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <!-- modal esercizio non uscito -->
<div class="modal text-matrix" tabindex="-1" id="modal_non_riuscita">
  <div class="modal-dialog bg-transparent-1 border-success ">
    <div class="modal-content bg-transparent-1 border-success ">
      <div class="modal-header bg-transparent-1 border-success ">
        <h5 class="modal-title bg-transparent-1 border-success ">Oh oh.... qualcosa è andato storto!</h5>
        <button type="button" class="btn-close btn-success" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body bg-transparent-1 border-success ">
        <p class="text-md-center"><img src="backgrounds/icon/wrong message.png" width="100" hight="100"></img></p>
      </div>
      <div class="modal-footer bg-transparent-1 border-success ">
        <button type="button" class="btn btn-success text-matrix" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php

    if ($id != "3") {
?>      
        <!--  contenuto pagine PHP e Javascript -->
        <main class="container-fluid bg-dark h-100 overflow-auto" id="wrapper">
            <div class="row h-100 "> 
                <div class="col overflow-auto h-100">
                    <div class="row overflow-auto prova h-100" id="testointro">
                        <div class="col p-4 text-white bg-transparent-1 overflow-auto" id="testoEsercizio">
                            <div>
                                <?php 
                                        if ($id=='1') {
                                            require_once(realpath(__DIR__)."/corpiLezione/introJS.html"); 
                                        }else {
                                            require_once(realpath(__DIR__)."/corpiLezione/introPHP.html"); 
                                        }
                                        ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100 d-d-none d-sm-block d-md-none"></div>
                <div class="col  h-100">
                    <div class="row h-50 bg-transparent-1" id="testoEsempio">
                    </div>
                    <div class="row h-50 bg-transparent-1" id="edit&risp">
                        <div class="row h-75 " >
                            <div class="col-10 overflow-auto " id="edit">
                                <div id="editor"><?php echo htmlspecialchars($editorText) ?></div> 
                            </div>
                            <div class="col-2 position-relative overflow-hidden"  id="buttons">
                                <div class="position-absolute top-50">
                                    <button type="button" class="btn btn-success text-matrix m-2 " id="reset" onclick="resetEditRisp()" >reset</button>
                                </div>
                                <div class="position-absolute bottom-0">
                                    <button type="button" class="btn btn-success text-matrix m-2 " id="invia" onclick="valutaCodice()">invia</button>
                                </div>
                            </div>
                        </div>      
                        <div class="row h-25 overflow-auto" id="risp">
                            <div class="col p-2">
                                <!-- <textarea class="text-xxl-start h-100 w-100" id="risposta" readonly="true">Qui vedi il risultato del codice!</textarea> -->
                                <div class='bg-light h-100' id="risposta"> <p>  <?php echo ($testoDivRisult) ?></p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<?php
    }else {
?> 
        <!-- contenuto pagina Lezioni Video -->
        <main class="container-fluid h-100 overflow-auto" id="wrapper">
            <div class="row h-75" id="testointro">
                <div class="col" id="testoEsercizio">
                    <div>
                        <p class="text-white"><?php require_once(realpath(__DIR__)."/corpiLezione/introVid.html") ?></p>
                    </div>
                </div>
            </div>
            <div class="flex-row h-25 " id="edit&risp">
                <div class="row h-100 p-3 overflow-auto" >
                    <div class="col-6 overflow-auto " id="edit">
                        <div id="editor"><?php echo htmlspecialchars($editorText) ?></div> 
                    </div>
                    <div class="col-1 position-relative overflow-hidden"  id="buttons">
                        <!-- possibile implementazione in futuro, non indispensabile ora -->
                        <!-- <div class="position-absolute top-0 start-50 translate-middle-x">
                            <button type="button" class="btn btn-outline-primary m-2 " id="cambiaLingua" >lang</button>
                        </div> -->
                        <div class="position-absolute top-50 start-50 translate-middle">
                            <button type="button" class="btn btn-success m-2 text-matrix " id="reset" onclick="resetEditRisp()" >reset</button>
                        </div>
                        <div class="position-absolute bottom-0 start-50 translate-middle-x ">
                            <button type="button" class="btn btn-success m-2 text-matrix " id="invia" onclick="valutaCodice()">invia</button>
                        </div>
                    </div>
                    
                    <div class="col-5 overflow-auto">
                        <!-- TODO scegliere una delle seguenti implementazioni -->
                        <!-- <textarea class="text-xxl-start h-100 w-100" id="risposta" readonly="true">qui vedi il risultato del codice!</textarea> -->
                        <!-- <pre class='bg-light h-100'><code id="risposta"> qui vedi il risultato del codice!</code></pre> -->
                        <div class='bg-light h-100 overflow-auto' id="risposta"> <p> <?php echo ($testoDivRisult) ?></p> </div>
                    </div>
                    
                </div>  
            </div>
        </main>
<?php  
    }
// aggiungo ora la parte di sidebar, essendo simile ad un modal, potrei metterla dove voglio, ovviamente dopo loadsidebar.php
    require_once(realpath(__DIR__).'/inc/sidebar.inc.php');
?>
        <script src='JS/mainScript.js'></script>
        <script> 
            function showSpin(nome){
                 $('#'+nome).show('slow').delay(1600).hide('slow');
                };
            setTheme("ace/theme/monokai"); 
            setLanguage("<?php echo $language ?>");
            setLangId(<?php echo $lanID ?>);
            setCode("<?php echo $cod ?>");
            setEditorText("<?php echo $editorText?>")
            setTestoDivResult("<?php echo ($testoDivRisult) ?>");
            ripristinaSessione();
        </script>
<?php
    require_once(realpath(__DIR__).'/inc/footer.inc.php');
?>
        