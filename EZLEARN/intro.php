<?php
	session_start();
	$GLOBALS['Title'] = "EZ-Intro";
?>
<!-- Pagina principale del sito -->
<!DOCTYPE html>
<html class="h-100">
	<head >
		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<title><?php echo $GLOBALS['Title']?></title>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<link rel='stylesheet' type='text/css' media='screen' href='css/main.css'>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<script src="JS/caricamentoProgresso.js"></script>
	</head>
	<body class="text-white d-flex flex-column h-100">
	<?php 
		//  inserisco la navbar
		require_once(realpath(__DIR__).'/inc/header.inc.php');
	?>
	<!-- contenuto della pagina principale con un intro al sito -->
		<main class="container-fluid h-100 overflow-auto" id="indexPage">
			<div class="row h-50">
				<div class="col h-100">
					<div class="container-fluid position-relative h-100 pt-2">
						<div class="card border-matrix w-75 position-relative top-0 start-50 translate-middle-x bg-transparent-1 border-matrix mt-4" > 
							<div class="card-header">			
								<p class="card-title display-5 text-center text-matrix"> <strong>Introduzione</strong> a learn to program! </p>
							</div>
							<div class="card-body bg-transparent-1">
								<p class="card-text text-center text-matrix"> 
									<!-- PHP e JS possono esser link alle pagine -->
									Qui ci saranno le istruzioni per come usare al meglio il sito <br>
									<br> 
									qui ci sarranno anche le FAQ e gif che mostrano come usare il sito la meglio!
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row h-50">
				<div class="col"> 
					
				</div>
			</div>

		</main>
		<!-- script per l'animazione della rotellina nella navbar -->
		<script>
			function showSpin(nome){
                 $('#'+nome).show('slow').delay(1600).hide('slow');
                };
		</script>
	<?php	
		require_once(realpath(__DIR__).'/inc/footer.inc.php');
	?> 