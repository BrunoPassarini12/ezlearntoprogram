
var editor = ace.edit("editor");
var expected_output =null;
var langId
var code 
var editorText
var resultTxtDef
let isLocale = false; // mettere true se si vuole utilizzare in locale
let machIp = "192.168.56.102"; // da modificare con l'indirizzo effettivo della macchina virtuale

function setTestoDivResult(txt) {
    resultTxtDef=txt;
}

function setEditorText(edTxt) {
    editorText = edTxt;
}

function ripristinaSessione() {
    if((id = localStorage.getItem(code)) != null)
        caricaContenutoDaSalvataggio(id);
}

function resetSessione() {
    if((id = localStorage.getItem(code)) != null)
        localStorage.removeItem(code)
}

function setTheme(tema) {
    editor.setTheme(tema);
}
function setLanguage(lang) {
    editor.session.setMode(lang);
}

function setLangId(lang) {
    langId = lang;
}

function setExpectedOut(expected) {
    expected_output = expected;
}

function setCode(cod) {
    code = cod;
}

if (isLocale) {
    function valutaCodice(){
        let codice = editor.getValue();
        $.ajax({
            url:"http://"+machIp+":2358/submissions/?base64_encoded=false&wait=true", 
            method: "POST",
            datatype: "json",
            data: {
                "source_code": codice,
                "language_id": langId,
                "expected_output": expected_output
            },
            success: function(data){
                if(data['stderr']==null){
                    $('#risposta').html(data['stdout'])
                    if (expected_output!=null) {
                        var resp = data['status'];
                    if (resp['id']==3) {
                        // tutto ok
                        var myModal = new bootstrap.Modal(document.getElementById('modal_riuscita'))
                        myModal.toggle();
                    }else if (resp['id']==4) {
                        // output non combacia!
                        var myModal = new bootstrap.Modal(document.getElementById('modal_non_riuscita'))
                        myModal.toggle();
                    }
                    }
                }else{
                    $('#risposta').text(data['stderr']) 
                }
            }
        });
    };
} else {
    // nota importante... facendo in questo modo si ha una latenza importate,  3/4/5s per avere una risposta!!!
    function valutaCodice() {
        let codice = btoa(editor.getValue());
        var encoded= JSON.stringify({ "source_code": codice, "language_id": langId, "expected_output": btoa(expected_output)})
        var settings = {
            async: true,
            crossDomain: true,
            url: "https://judge0-ce.p.rapidapi.com/submissions/?base64_encoded=true&wait=true",
            method: "POST",
            headers: {
                "content-type": "application/json",
                "x-rapidapi-key": "0a91f1080fmsheb1887f28e72236p1c8f71jsn9b3fa4e0454a",
                "x-rapidapi-host": "judge0-ce.p.rapidapi.com"
            },
            processData: false,
            data: encoded
        };

        $.ajax(settings).done(function (data1) {
            if(data1['stderr']==null){
                $('#risposta').html(atob(data1['stdout']))
                if (expected_output!=null) {
                    var resp = data1['status'];
                if (resp['id']==3) {
                    // tutto ok
                    var myModal = new bootstrap.Modal(document.getElementById('modal_riuscita'))
                    myModal.toggle();
                }else if (resp['id']==4) {
                    // output non combacia!
                    var myModal = new bootstrap.Modal(document.getElementById('modal_non_riuscita'))
                    myModal.toggle();
                }
                }
            }else{
                $('#risposta').text(atob(data1['stderr'])) 
            }
        });
        
    }
}

function caricaContenuto(ref) {
    var ident = $(ref).attr('id');
    $("#testoEsercizio").load("azioni/loadLesson.php" ,
        {
            tipo : ident
        }
    
    );
    localStorage.setItem( code , ident );
}

function caricaContenutoDaSalvataggio(ident) {
    $("#testoEsercizio").load("azioni/loadLesson.php" ,
        {
            tipo : ident
        }

    );
    localStorage.setItem(code, ident );
}

function aggiornaTitolo(titolo) {
    $('#titolo').text(titolo);
}    

function inviaProgresso(){
    var daSalv = {};
    for (let index = 0;  index < localStorage.length; index++){
        daSalv[""+localStorage.key(index)] = localStorage.getItem(localStorage.key(index)); 
    }
    localStorage.clear();
    $.ajax({
        url: 'azioni/updateDB.php',
        type: 'POST',
        data:  {"salvataggio" : JSON.stringify(daSalv)}
    })
    
}

function resetEditRisp() {
    editor.session.setValue(editorText);
    $('#risposta').text("Qui vedi il risultato del codice!");
}

function resetExpectedOut(){
    expected_output = null;
}