// questa funzione carica nella local storage i progressi ricevuti dal server in base all'utente che ha fatto il login
function aggiornaProgressoLocale(progress) {
    pro = JSON.parse(progress);
    localStorage.clear();
    for(var i in pro) {
        localStorage.setItem(i, pro[i]);
    };
}
// questa funzione, chiamata quando si schiaccia il tasto log-out, manda i progressi fatti durante la sessione
// autenticata al server, i progressi sono salvati durante la sessione nella localStorage
function inviaProgresso(){
    var daSalv = {};
    for (let index = 0;  index < localStorage.length; index++){
        daSalv[""+localStorage.key(index)] = localStorage.getItem(localStorage.key(index)); 
    }
    localStorage.clear();
    $.ajax({
        url: 'azioni/updateDB.php',
        type: 'POST',
        data:  {"salvataggio" : JSON.stringify(daSalv)}
    })
    
}


    
