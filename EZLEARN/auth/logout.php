<!-- script per il logout, distrugge la sessione dando tempo allo script in js di caricare il salvataggio sul server -->
<?php 
    session_start();
    sleep(1);
    $_SESSION = array();
    session_destroy();
    header('Location: ../index.php');
?>