<?php
// controllo se l'utente esiste nel database
    include_once(realpath(__DIR__)."../../conf/config.inc.php");
    session_start();
    if (isset($_SESSION['utente'])) {
        header("Location: ../index.php");
    }
    $authenticationError=false;
    if (isset($_POST['email'])) {
// controllo il campo email di post è legittimo
        if (preg_match("/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i",$_POST['email'])) {

            $sql = 'select *  from user where `email` = "'.$_POST['email'].'";';

            $result = mysqli_query($conn, $sql);
            $utente	=	$result->fetch_assoc();
    // se la password inserita nel form corrisponde a quella presente nel database in corrispondenza della mail inserita
    // allora inizia la sessione
            if (password_verify($_POST['pass'],$utente['password'])) {

                $_SESSION['utente'] = $utente['username'];
                $_SESSION['refresh'] = 0;
                
                $sql = 'select descrizione_progresso from progresso 
                left join user on progresso.idprogresso = user.progresso_idprogresso
                where username = "'.$_SESSION['utente'].'";';

                $result = mysqli_query($conn, $sql);
                $progresso = $result->fetch_assoc();

                $_SESSION['progresso']= $progresso['descrizione_progresso'];
                $_SESSION['master']=$utente['master'];

                header("Location: ../index.php");
            }else {
                $authenticationError=true;
            }
        }else {
            $authenticationError=true;
        }
    }

    $GLOBALS['Title'] = 'Login';
    
?>
<!-- pagina del login -->
<!DOCTYPE html>
<html class="h-100">
	<head >
		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<title>EZ - <?php echo($GLOBALS['Title']) ?></title>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<link rel='stylesheet' type='text/css' media='screen' href='../css/main.css'>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
	</head>
	<body class="bg-primary text-white d-flex flex-column h-100">
<!-- navbar -->
<?php 

    require_once(realpath(__DIR__).'/inc/header.inc.php');

    if ($authenticationError) {
?>
<!-- banner autenticazione fallita -->
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
            </symbol>
        </svg>
        <div class="alert alert-danger d-flex align-items-center mb-0" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24"><use xlink:href="#exclamation-triangle-fill"/></svg>
            <div>
                Autenticazione non riuscita, Email o Password errate.
            </div>
        </div>
<?php         
    }elseif (isset($_SESSION['firstTime'])) {
?>
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
            </symbol>
        </svg>
        <div class="alert alert-primary d-flex align-items-center mb-0" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24"><use xlink:href="#info-fill"/></svg>
            <div>
                Registrazione avvenuta con sucesso. Procedere con il login!
            </div>
        </div>
<?php
    }
?>
<!-- form per l'autenticazione -->
        <main class="container-fluid h-100 overflow-auto" id="wrapper">
            <div class='container position-absolute start-50 top-50 translate-middle'>
                <form class="needs-validation" action="login.php" method="POST" novalidate>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationEmail">E-mail</label>
                            <div class="input-group">
                                <input type="email" name="email" class="form-control" id="validationEmail" pattern='^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$' placeholder="E-mail" aria-describedby="inputGroupPrepend" required>
                                <div class="invalid-feedback">
                                    Per favore inserire una e-mail valida.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="validationPsw">Password</label>
                            <input type="password" name="pass" class="form-control" id="validationPsw" placeholder="Password" required>
                            <div class="invalid-feedback">
                                password non valida!
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success text-matrix" onclick="showSpin('spin-btn-inv')" type="submit"><span class="spinner-border spinner-border-sm " style="display:none" role="status" aria-hidden="true" id="spin-btn-inv"></span>Login</button>
                </form>
            </div>
            <!-- script per la valutazione del form prima di inviare il contenuto al server per il confronto con il db -->
            <script>
                function showSpin(nome){
                 $('#'+nome).show('slow').delay(1600).hide('slow');
                };
                (function() {
                'use strict';
                window.addEventListener('load', function() {
                    var forms = document.getElementsByClassName('needs-validation');
                    var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                    });
                }, false);
                })();
            </script>
        </main>
<!-- footer -->
<?php 

    require_once(realpath(__DIR__).'../../inc/footer.inc.php');
?>