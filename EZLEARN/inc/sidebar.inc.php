<!-- Siedebar -->
<div class="offcanvas offcanvas-start" data-bs-scroll="true" tabindex="-1" id="offcanvasWithBothOptions" aria-labelledby="titolosidebar">
  <div class="offcanvas-header bg-transparent-2 border-bottom border-success text-matrix" id="headoff">
    <a href="Lezioni.php?id=<?php echo $id ?>" onclick="resetSessione()" class="text-center link-dark text-decoration-none text-matrix" id="titolosidebar">
      <span class="fs-5 fw-semibold ml-2 pl-5"><i class="bi bi-archive pl-lg-1"></i>        Lessons
      </span> 
    </a>
    <button type="button" class="btn-close text-reset bg-transparent-2" data-bs-dismiss="offcanvas" aria-label="Close"></button> 
  </div>
  <?php 
        if ($id != '3') {
         
  ?>
  <!-- sidebar per le pagine PHP e JavaScript -->
  <div class="accordion accordion-flush bg-transparent-2 " id="accordionFlushExample">
  <div class="accordion-item bg-transparent ">
    <h2 class="accordion-header bg-transparent" id="flush-headingOne">
      <button class="accordion-button collapsed bg-transparent-2  text-matrix" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
        Theory
      </button>
    </h2>
    <div id="flush-collapseOne" class="accordion-collapse collapse bg-transparent" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body bg-transparent ">
        <?php
          $cap = 0;
          foreach ($teoria as $key => $value) {
            $lessonNumber = explode('.', $key);
            if ( $cap != $lessonNumber[0]) {
              $cap = $lessonNumber[0];
              echo '<a class="list-group-item list-group-item-action bg-transparent-2 text-matrix" id="teo-'.$key.'-'.$cod.'" type="button"  data-bs-dismiss="offcanvas" aria-label="Close" onclick="caricaContenuto(this)">'.$key.' '.$value.'</a>';
            }else {
              echo '<a class="list-group-item list-group-item-action bg-transparent-2 text-matrix " id="teo-'.$key.'-'.$cod.'" type="button" data-bs-dismiss="offcanvas" aria-label="Close" onclick="caricaContenuto(this)">&nbsp;&nbsp;&nbsp;&nbsp;'.$key.' '.$value.'</a>';
            }
          }
        ?>  
      </div>
    </div>
  </div>
  <div class="accordion-item bg-transparent">
    <h2 class="accordion-header bg-transparent" id="flush-headingOne">
      <button class="accordion-button collapsed bg-transparent-2 text-matrix" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseOne">
        Exercices
      </button>
    </h2>
    <div id="flush-collapseTwo" class="accordion-collapse collapse bg-transparent" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body bg-transparent">
        <?php
          $cap = 0;
          foreach ($esercizi as $key => $value) {
            $lessonNumber = explode('.', $key);
            if ( $cap != $lessonNumber[0]) {
              $cap = $lessonNumber[0];
              echo '<a class="list-group-item list-group-item-action bg-transparent-2 text-matrix" id="ese-'.$key.'-'.$cod.'" type="button" data-bs-dismiss="offcanvas" aria-label="Close" onclick="caricaContenuto(this)">'.$key.' '.$value.'</a>';
            }else {
              echo '<a class="list-group-item list-group-item-action bg-transparent-2 text-matrix" id="ese-'.$key.'-'.$cod.'" type="button" data-bs-dismiss="offcanvas" aria-label="Close" onclick="caricaContenuto(this)">&nbsp;&nbsp;&nbsp;&nbsp;'.$key.' '.$value.'</a>';
            }
          }
        ?>
      </div>
    </div>
  </div>
</div>
  <?php 
        }else {
          foreach ($videoLezioni as $key => $value) {
  ?>
  <!-- sidebar per pagina Lezioni Video -->
  <div class="accordion accordion-flush bg-transparent-2" id="accordionFlushExample">
  <div class="accordion-item bg-transparent">
    <h2 class="accordion-header bg-transparent" id="flush-headingOne">
      <button class="accordion-button collapsed bg-transparent-2 text-matrix" type="button" data-bs-toggle="collapse" data-bs-target="#<?php echo $key?>-collapseOne" aria-expanded="false" aria-controls="<?php echo $key?>-collapseOne">
        <?php echo $key ?>
      </button>
    </h2>
    <div id="<?php echo $key?>-collapseOne" class="accordion-collapse collapse bg-transparent-2 text-matrix" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
      <div class="accordion-body bg-transparent">
        <?php
          $cap = 0;
          foreach ($videoLezioni[$key] as $key1 => $value1) {
            $lessonNumber = explode('.', $key1);
            if ( $cap != $lessonNumber[0]) {
              $cap = $lessonNumber[0];
              echo '<a class="list-group-item list-group-item-action bg-transparent-2 text-matrix" id="vid-'.$key1.'-'.$key.'" type="button" data-bs-dismiss="offcanvas" aria-label="Close" onclick="caricaContenuto(this)">'.$key1.' '.$value1.'</a>';
            }else {
              echo '<a class="list-group-item list-group-item-action bg-transparent-2 text-matrix " id="vid-'.$key1.'-'.$key.'" type="button" data-bs-dismiss="offcanvas" aria-label="Close" onclick="caricaContenuto(this)">&nbsp;&nbsp;&nbsp;&nbsp;'.$key1.' '.$value1.'</a>';
            }
          }
        ?>
      </div>
    </div>
  </div>
</div>            
  <?  
          }
        }
  ?>
</div>