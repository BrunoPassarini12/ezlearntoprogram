
        <!-- NavBar -->
        <header class="" >
            <nav class="navbar navbar-expand-lg navbar-light border-bottom border-success" id="header">
                <div class="container-fluid">
                <a class="navbar-brand text-matrix" href="index.php">EZ</a>
                <button class="navbar-toggler bg-success" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link text-matrix" href="intro.php">Lessons Intro</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-matrix" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Languages
                        </a>
                        <ul class="dropdown-menu bg-black text-matrix" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item text-matrix" href="Lezioni.php?id=1">JavaScript</a></li>
                            <li><a class="dropdown-item text-matrix" href="Lezioni.php?id=2">PHP</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item text-matrix" href="Lezioni.php?id=3">Video Lessons</a></li>
                        </ul>
                    </li>
                    </ul>
                    <!-- se l'utente ha fatto il login, mostro il link al log-out altrimenti il contrario -->
                    <?php
                         if(isset($_SESSION['utente'])){
                            if ($_SESSION['master']) {
                    ?>
                    <!-- se l'utente è anche master mostro il pulsante per la pagina all'inserimento delle lezioni -->
                                <div class="me-2 text-matrix">
                                    <span class="spinner-border spinner-border-sm " style="display:none" role="status" aria-hidden="true" id="spin-add-lesson"> </span>
                                    <a class="btn btn-outline-success text-matrix" onclick="showSpin('spin-add-lesson')" href="admin/insLesson.php"  id="add-lesson"> Add lesson </a>
                                </div>
                    <?php
                            }
                    ?>
                   
                    <div class="me-2 text-matrix">
                        <span class="spinner-border spinner-border-sm " style="display:none" role="status" aria-hidden="true" id="spin-logout"> </span>
                        <a class="btn btn-outline-success text-matrix" onclick="showSpin('spin-logout');inviaProgresso();" href="auth/logout.php"  id="log-out"> Logout </a>
                    </div>

                    <?php
                         }else {
                    ?>         
                            <div class="me-2 text-matrix" >
                                <span class="spinner-border spinner-border-sm " style="display:none" role="status" aria-hidden="true" id="spin-login"> </span>
                                <a class="btn btn-outline-success text-matrix" onclick="showSpin('spin-login')" href="auth/login.php" id="log-in"> Login </a>
                            </div>
                            <div class="me-2 text-matrix" >
                                <span class="spinner-border spinner-border-sm " style="display:none" role="status" aria-hidden="true" id="spin-sign-up"> </span>
                                <a class="btn btn-outline-success text-matrix" onclick="showSpin('spin-sign-up')" href="auth/createAcc.php" id="sign-up"> Sign-up </a>
                            </div>
                    <?php        
                         }     
                            
                    ?>
                    <!-- se il titolo della pagina è diverso da quello della pagina di log-in o iniziale, allora mostro il tasto per la sidebar -->
                    <?php
                     if (($GLOBALS['Title'] != "EZ") && ($GLOBALS['Title'] != "Login") && ($GLOBALS['Title'] != "EZ-Intro") ) {
                         echo('
                                <div class="me-2"> 
                                    <button class="btn btn-outline-success text-matrix " type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasWithBothOptions" aria-controls="offcanvasWithBothOptions" id="button_menu">
                                        Lessons Menù 
                                    </button>   
                                </div>
                        ');
                     } 
                    ?>
                </div>
                </div>
            </nav>
        </header>