
<!-- contenuto della lezione -->

<div class="overflow-auto h-100"> 

    <p class="h1 text-center text-capitalize">  
        <?php echo($contenuto['titolo']) ?>
    </p>
        
    <div class="overflow-auto"> 
        <?php echo urldecode($contenuto['corpo']); ?>
    </div>
</div>