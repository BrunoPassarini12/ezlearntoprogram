<div id="intro-whatis" class="section">
    <div class="info"><h1 class="title">What is PHP?</h1></div>
    <p class="para">
     <acronym title="PHP: Hypertext Preprocessor">PHP</acronym> (recursive acronym for <code class="literal">PHP: Hypertext
     Preprocessor</code>) is a widely-used open source general-purpose
     scripting language that is especially suited for web
     development and can be embedded into HTML.
    </p>
    <p class="para">
     Nice, but what does that mean? An example:
    </p>
    <p class="para">
     </p><div class="example" id="example-1">
      <div class="info"><p><strong>Example #1 An introductory example</strong></p></div>
      <div class="example-contents">
 <div class="phpcode"><code><span style="color: #14cc23">
 &lt;!DOCTYPE&nbsp;html&gt;<br>&lt;html&gt;<br>&nbsp;&nbsp;&nbsp;&nbsp;&lt;head&gt;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;title&gt;Example&lt;/title&gt;<br>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/head&gt;<br>&nbsp;&nbsp;&nbsp;&nbsp;&lt;body&gt;<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: #0000BB">&lt;?php<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #d1e612">echo&nbsp;</span><span style="color: #DD0000">"Hi,&nbsp;I'm&nbsp;a&nbsp;PHP&nbsp;script!"</span><span style="color: #dae417">;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #0000BB">?&gt;<br></span><br>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/body&gt;<br>&lt;/html&gt;</span>
 </code></div>
      </div>
 
     </div>
    <p></p>
    <p class="para">
     Instead of lots of commands to output HTML (as seen in C or Perl),
     PHP pages contain HTML with embedded code that does
     "something" (in this case, output "Hi, I'm a PHP script!").
     The PHP code is enclosed in special start and end processing
     instructions <code class="code">&lt;?php</code> and <code class="code">?&gt;</code>
     that allow you to jump into and out of "PHP mode."
    </p>
    <p class="para">
     What distinguishes PHP from something like client-side JavaScript
     is that the code is executed on the server, generating HTML which
     is then sent to the client. The client would receive
     the results of running that script, but would not know
     what the underlying code was. You can even configure your web server
     to process all your HTML files with PHP, and then there's really no
     way that users can tell what you have up your sleeve.
    </p>
    <p class="para">
     The best things in using PHP are that it is extremely simple
     for a newcomer, but offers many advanced features for
     a professional programmer. Don't be afraid reading the long
     list of PHP's features. You can jump in, in a short time, and
     start writing simple scripts in a few hours.
    </p>
    <p class="para">
     Although PHP's development is focused on server-side scripting,
     you can do much more with it. Read on, and see more in the
     What can PHP do? section,
     or go right to the introductory
     tutorial if you are only interested in web programming.
    </p>
   </div>


   <div id="intro-whatcando" class="section">
    <div class="info"><h1 class="title">What can PHP do?</h1></div>
    <p class="para">
     Anything. PHP is mainly focused on server-side scripting,
     so you can do anything any other CGI program can do, such
     as collect form data, generate dynamic page content, or
     send and receive cookies. But PHP can do much more.
    </p>
    <p class="para">
     There are three main areas where PHP scripts are used.
     </p><ul class="itemizedlist">
      <li class="listitem">
       <span class="simpara">
        Server-side scripting. This is the most traditional
        and main target field for PHP. You need three things
        to make this work: the PHP parser (CGI or server
        module), a web server and a web browser. You need to
        run the web server, with a connected PHP installation.
        You can access the PHP program output with a web browser,
        viewing the PHP page through the server. All these can
        run on your home machine if you are just experimenting
        with PHP programming. See the
       installation instructions
        section for more information.
       </span>
      </li>
      <li class="listitem">
       <span class="simpara">
        Command line scripting. You can make a PHP script
        to run it without any server or browser.
        You only need the PHP parser to use it this way.
        This type of usage is ideal for scripts regularly
        executed using cron (on *nix or Linux) or Task Scheduler (on
        Windows). These scripts can also be used for simple text
        processing tasks. See the section about
        Command line usage of PHP
        for more information.
       </span>
      </li>
      <li class="listitem">
       <span class="simpara">
        Writing desktop applications. PHP is probably
        not the very best language to create a desktop
        application with a graphical user interface, but if
        you know PHP very well, and would like to use some
        advanced PHP features in your client-side applications
        you can also use PHP-GTK to write such programs. You also
        have the ability to write cross-platform applications this
        way. PHP-GTK is an extension to PHP, not available in
        the main distribution. If you are interested
        in PHP-GTK, visit »&nbsp;its
        own website.
       </span>
      </li>
     </ul>
    <p></p>
    <p class="para">
     PHP can be used on all major operating systems, including
     Linux, many Unix variants (including HP-UX, Solaris and OpenBSD),
     Microsoft Windows, macOS, RISC OS, and probably others.
     PHP also has support for most of the web servers today. This
     includes Apache, IIS, and many others. And this includes any
     web server that can utilize the FastCGI PHP binary, like lighttpd
     and nginx. PHP works as either a module, or as a CGI processor.
    </p>
    <p class="para">
     So with PHP, you have the freedom of choosing an operating
     system and a web server. Furthermore, you also have the choice
     of using procedural programming or object oriented
     programming (OOP), or a mixture of them both.
    </p>
    <p class="para">
     With PHP you are not limited to output HTML. PHP's abilities
     includes outputting images, PDF files and even Flash movies
     (using libswf and Ming) generated on the fly. You can also
     output easily any text, such as XHTML and any other XML file.
     PHP can autogenerate these files, and save them in the file
     system, instead of printing it out, forming a server-side
     cache for your dynamic content.
    </p>
    <p class="para">
     One of the strongest and most significant features in PHP is its
     support for a wide range of databases. 
     Writing a database-enabled web page is incredibly simple using one of
     the database specific extensions (e.g., formysql),
     or using an abstraction layer like PDO, or connect
     to any database supporting the Open Database Connection standard via the
     ODBC extension. Other databases may utilize
     cURL or sockets,
     like CouchDB.
    </p>
    <p class="para">
     PHP also has support for talking to other services using protocols
     such as LDAP, IMAP, SNMP, NNTP, POP3, HTTP, COM (on Windows) and
     countless others. You can also open raw network sockets and
     interact using any other protocol. PHP has support for the WDDX
     complex data exchange between virtually all Web programming
     languages. Talking about interconnection, PHP has support for
     instantiation of Java objects and using them transparently
     as PHP objects.
    </p>
    <p class="para">
     PHP has useful text processing features,
     which includes the Perl compatible regular expressions (PCRE),
     and many extensions and tools to parse and access XML documents.
     PHP standardizes all of the XML extensions on the solid base of libxml2,
     and extends the feature set adding SimpleXML,
     XMLReader and XMLWriter support.
    </p>
    <p class="para">
     And many other interesting extensions exist, which are categorized both
     alphabetically and by category.
     And there are additional PECL extensions that may or may not be documented
     within the PHP manual itself, like»&nbsp;XDebug.
    </p>
    <p class="para">
     As you can see this page is not enough to list all
     the features and benefits PHP can offer. Read on in
     the sections about installing
     PHP, and see the  function
     reference part for explanation of the extensions
     mentioned here.
    </p>
   </div>