# EzLearnToProgram

## Overview
L'applicazione EzLearnToProgram è per chiunque voglia avvicinarsi alla programmazione per impararne le basi e vederne i potenziali.

### Scope
Ha lo scopo di insegnare i costrutti fondamentali della programmazione e di facilitarne la comprensione.

Si ha infatti la possibilità di poter "giocare" con gli esempi delle lezioni grazie ad un editor integrato che permette di eseguire il codice senza dover lasciare la pagina o la lezione.


### Features


Vi sarà una parte tutorial per diversi linguaggi di programmazione, sia teoria che esercizi, e la possibilità di provare a scrivere frammenti di codice direttamente nella pagina stessa. 

La parte degli esercizi avrà una valutazione dell'output previsto dall'esercizio con un commento in caso di riuscita.


Vi sarà una parte e-learning con videolezioni e approfondimenti, con la possibilità di provare direttamente il codice mentre si segue la lezione, senza dover lasciare la pagina.


Vi sarà la possibilità di avere un proprio account dove verranno salvati i progressi.