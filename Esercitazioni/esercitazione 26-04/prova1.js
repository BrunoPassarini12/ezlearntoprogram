const express = require('express');
const app = express();
const port = 3005;
const bodyParser = require('body-parser');
const fs = require('fs');
const demoFileName = './demofile.json';

app.use(bodyParser.json()); // for application/json
app.use(bodyParser.urlencoded({ extended: true })); // for application/x-www-form-urlencoded

app.get('/file', function (req, res) {
    fs.readFile(demoFileName, (err, data) => { // async!!
        if (err) throw err;
        res.send(data)
    });
})

app.post('/file', function (req, res) {
    m = req.body;
    fs.readFile(demoFileName, (err, data) => { // async!!
        if (err) throw err;
        jsonData = JSON.parse(data);
        const merged = JSON.stringify({ ...jsonData, ...m }, null, 2);

        fs.writeFile(demoFileName, merged, (err) => { // async!!
            if (err) throw err;
            res.send(merged);
        });
    });
})

app.put('/file', function(req, res){
    const nuovo = JSON.stringify(m.body);
    fs.writeFile(demoFileName, nuovo, (err) => { // async!!
        if (err) throw err;
        res.send(nuovo);
    });

})
app.delete('/file', function(req, res) {
    const vuoto = JSON.stringify({});
    fs.writeFile(demoFileName, vuoto, (err) => { // async!!
        if (err) throw err;
        res.send(vuoto);
    });
})

app.listen(port, () => console.log(`MyApp05 listening at http://localhost:${port}`))