# Tecnologie per le applicazioni web
Progetto per l'esame di Tecnologie per le applicazioni web.

## Stato
Ora come ora le richieste per la risoluzione del codice vengono mandate ad un server Judge0 hostato in Rapid API che ha un hard-limit di 100 al giorno e una latenza di circa 4/5s.
Se si vuole migliorare la latenza ed avere illimitate possibili richieste consiglio di seguire [l'installazione off-line.](#inst-off)


## <a name="inst-off">Installazione Judge0 per test off-line </a>
Per replicare la mia installazione seguire quanto segue.
Per la configurazione del file mainScript.js seguire le istruzioni [qui](#config).


### Installazione Linux 
Ho proceduto ad installare Linux in una Virtual Machine, io ho usato [VirtualBox](https://www.oracle.com/it/virtualization/technologies/vm/downloads/virtualbox-downloads.html).
La versione Linux scelta da me è [Ubuntu 21.04 Desktop](https://www.ubuntu-it.org/download), ma in realtà può essere una delle versioni supportate da [Docker](https://docs.docker.com/engine/install/).
> Nota! nel mio caso ho impostato la macchina perché avesse un disco rigido di max 50GB, alla fine della procedura di installazione la macchina virtuale porta via 24GB di spazio, ma in base alla versione di Linux  scelta, questo può cambiare. Io consiglio di impostare almeno 30GB di spazio. 


### Installazione Docker e Docker Compose
Una volta installato il sistema operativo, si procede con l'installazione di [Docker](https://docs.docker.com/engine/install/) e [Docker Compose](https://docs.docker.com/compose/install/) seguendo le istruzioni presenti nei link ed installando in ordine [Docker](https://docs.docker.com/engine/install/) -> [Docker Compose](https://docs.docker.com/compose/install/).


### Installazione Judge0
Molto semplicemente basta seguire le istruzioni presenti nella [pagina di deployment-procedure](https://github.com/judge0/judge0/blob/master/CHANGELOG.md#deployment-procedure).


### Configurazione VirtualBox
Nel caso si utilizzi VirtualBox, nelle impostazioni riguardanti la Macchina virtuale su cui è installata l'API, bisogna modificare l'impostazione della scheda di rete perché sia : 

```
    Connessa a : Scheda solo host
           Nome: VirtualBox Host-Only Ethernet Adapter
```

>  ora la macchina virtuale dovrebbe segnare la connessione ad internet, ma non riuscire più a caricare una pagina web.

A questo punto in Linux con il comando  `ifconfig`, inserito nel terminale, si ottiene l'indirizzo ip della Virtual Machine su cui opera l'API.


### <a name="config"> Configurazione file mainScript.js</a>

#### Attivare la funzione valutaCodice() versione offline
Per far si che il sito si rivolga alla macchina virtuale per la risoluzione del codice, basta modificare due variabili:
```
let isLocale = false; // mettere true se si vuole utilizzare in locale
let machIp = "192.168.56.102"; // da modificare con l'indirizzo effettivo della macchina virtuale
```




